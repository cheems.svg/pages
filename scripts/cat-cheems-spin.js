const states = [
    {clockwise: true, speed: 1},
    {clockwise: true, speed: 2},
    {clockwise: true, speed: 3},
    {clockwise: true, speed: 4},
    {clockwise: false, speed: 1},
    {clockwise: false, speed: 2},
    {clockwise: false, speed: 3},
    {clockwise: false, speed: 4},
]

document.addEventListener("DOMContentLoaded", () => {
    const el = document.getElementsByTagName('img')[0];
    let current = 0;
    const applyClasses = () => {
        const {clockwise, speed} = states[Math.round(current % states.length)];
        for (let i=1; i <= 4; i++) {
            const cls = `speed${i}`;
            if (i === speed) {
                el.classList.add(cls);
            } else {
                el.classList.remove(cls);
            }
        }
        if (clockwise) {
            el.classList.add('clockwise');
            el.classList.remove('counterClockwise');
        } else {
            el.classList.add('counterClockwise');
            el.classList.remove('clockwise');
        }
    }

    document.body.addEventListener("mousedown", () => { current += 1; applyClasses(); });
    applyClasses();
});